import { API_HOST_LOCAL } from "../utils/constants";

export async function getUsuarioCuentaApi() {
  try {
    console.log("Servicio");
    const url = `${API_HOST_LOCAL}/findCuenta`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    throw error;
  }
}

export async function getCuentaDetailsApi(id) {
  try {
    const url = `${API_HOST_LOCAL}/findMovimientoByCuenta/${id}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    throw error;
  }
}

export async function getUsuarioApi(id) {
  try {
    const url = `${API_HOST_LOCAL}/findUsuarioById/${id}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    throw error;
  }
}

export async function getTotalApi(id) {
  try {
    const url = `${API_HOST_LOCAL}/findUsuarioTotalById/${id}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    throw error;
  }
}

export async function sendCuentaApi() {
  try {
    console.log("ENtro servicio");
    const url = `${API_HOST_LOCAL}/createCuenta`;

    let params = {
      nombre: "PRUEBA",
      descripcion: "SOY UNA DESCRIPCION",
      tipo: "I",
      id_usuario: "1",
    };
    fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    });
    return "OK";
  } catch (error) {
    throw error;
  }
}
