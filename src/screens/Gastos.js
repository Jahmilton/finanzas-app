import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
} from "react-native";
import useAuth from "../hooks/useAuth";

export default function Gastos() {
  const { auth, logout } = useAuth();
  console.log("AUT", auth);

  return (
    <View>
      <Text style={styles.title}>Nuevo gasto</Text>
      <TextInput
        placeholder="Nombre"
        style={styles.input}
        autoCapitalize="none"
      />
      <TextInput
        placeholder="Descripción"
        style={styles.input}
        autoCapitalize="none"
        multiline={true}
        numberOfLines={4}
      />

      <Button title="Guardar" color="#FA6C6C" />
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontSize: 28,
    fontWeight: "bold",
    marginTop: 50,
    marginBottom: 15,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
  },
  error: {
    textAlign: "center",
    color: "#f00",
    marginTop: 20,
  },
});
