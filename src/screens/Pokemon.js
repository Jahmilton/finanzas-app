import React, { useState, useEffect } from "react";
import {
  ScrollView,
  View,
  Button,
  TextInput,
  Text,
  StyleSheet,
  SafeAreaView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { getCuentaDetailsApi } from "../api/cuentas";
import Header from "../components/Pokemon/Header";
import Type from "../components/Pokemon/Type";
import Stats from "../components/Pokemon/Stats";
import Favorite from "../components/Pokemon/Favorite";
import useAuth from "../hooks/useAuth";
import { forEach } from "lodash";

export default function Pokemon(props) {
  const {
    navigation,
    route: { params },
  } = props;
  const [cuenta, setCuenta] = useState(null);
  const { auth } = useAuth();
  console.log("CUENTA DETAILS: ", params.nombre);

  useEffect(() => {
    (async () => {
      try {
        const data = await getCuentaDetailsApi(params.id);
        data.forEach((item) => {
          console.log("FEchssssa", item.fecha);
        });
        setCuenta(data);
      } catch (error) {
        navigation.goBack();
      }
    })();
  }, [params]);
  const arr = [{ descripcion: params.descripcion }];
  if (!cuenta) return null;

  return (
    <ScrollView>
      <Header
        name={params.nombre}
        order={params.id}
        image="imagen"
        type={params.tipo}
      />
      <Type types={arr} />
      <Stats stats={cuenta} />
      <SafeAreaView>
        <Text style={styles.title}>Nuevo movimiento</Text>
        <TextInput
          placeholder="Descripción"
          style={styles.input}
          autoCapitalize="none"
        />
        <TextInput
          placeholder="Valor"
          style={styles.input}
          autoCapitalize="none"
          multiline={true}
          numberOfLines={4}
        />

        <Button title="Guardar" color="#C1C1C1" />
      </SafeAreaView>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontSize: 28,
    fontWeight: "bold",
    marginTop: 50,
    marginBottom: 15,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
  },
  error: {
    textAlign: "center",
    color: "#f00",
    marginTop: 20,
  },
});
