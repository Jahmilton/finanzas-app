import React, { useState, useEffect } from "react";
import { sendCuentaApi } from "../api/cuentas";
import {
  SafeAreaView,
  Text,
  StyleSheet,
  View,
  TextInput,
  Button,
} from "react-native";
import useAuth from "../hooks/useAuth";

export default function Ingresos() {
  const { auth, logout } = useAuth();
  console.log("AUT", auth);

  handleClick = () => {
    alert("Button clicked!");
    const response = sendCuentaApi();
  };

  return (
    <View>
      <Text style={styles.title}>Nuevo ingreso</Text>
      <TextInput
        placeholder="Nombre"
        style={styles.input}
        autoCapitalize="none"
      />
      <TextInput
        placeholder="Descripción"
        style={styles.input}
        autoCapitalize="none"
        multiline={true}
        numberOfLines={4}
      />

      <Button title="Guardar" color="#48CFB2" onPress={this.handleClick} />
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    textAlign: "center",
    fontSize: 28,
    fontWeight: "bold",
    marginTop: 50,
    marginBottom: 15,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 10,
  },
  error: {
    textAlign: "center",
    color: "#f00",
    marginTop: 20,
  },
});
