import React, { useState, useEffect } from "react";
import { SafeAreaView, Text } from "react-native";
import { getPokemonsApi, getPokemonDetailsByUrlApi } from "../api/pokemon";
import { getUsuarioCuentaApi, getUsuarioApi } from "../api/cuentas";
import PokemonList from "../components/PokemonList";
import CuentasList from "../components/CuentasList";
import useAuth from "../hooks/useAuth";

export default function Cuentas() {
  const [pokemons, setPokemons] = useState([]);
  const [nextUrl, setNextUrl] = useState(null);

  const { auth, logout } = useAuth();
  console.log("AUT", auth);
  console.log("LOGOUT", logout);

  useEffect(() => {
    (async () => {
      await loadPokemons();
    })();
  }, []);

  const loadPokemons = async () => {
    try {
      if (auth) {
        console.log("SI --------------------------");
      } else {
        console.log("NO -------------------------");
      }
      const data = await getUsuarioCuentaApi();
      console.log("Cuentas: ", data);
      const response = await getUsuarioApi(2);
      console.log("responseresponseresponse: ", response);
      setPokemons([...pokemons, ...data]);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <SafeAreaView>
      <CuentasList pokemons={pokemons} />
      <Text>...</Text>
    </SafeAreaView>
  );
}
