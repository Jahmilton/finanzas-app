import React from "react";
import { StyleSheet, View, Text } from "react-native";
import { map, capitalize } from "lodash";

export default function Stats(props) {
  const { stats } = props;
  const fec = new Date();
  console.log("STATS: ", stats);
  return (
    <View style={styles.content}>
      <Text style={styles.title}>Movimientos</Text>
      {map(stats, (item, index) => (
        <View key={index} style={styles.block}>
          <View style={styles.blockTitle}>
            <Text style={styles.statName}>{capitalize(item.descripcion)}</Text>
          </View>
          <View style={styles.blockInfo}>
            <Text style={styles.number}>$ {item.valor} COP</Text>
            <Text style={styles.date}>Fecha: {item.fecha}</Text>
          </View>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  content: {
    paddingHorizontal: 20,
    marginTop: 40,
    marginBottom: 80,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    paddingBottom: 5,
  },
  block: {
    flexDirection: "row",
    paddingVertical: 5,
  },
  blockTitle: {
    width: "40%",
  },
  statName: {
    fontSize: 11,
    color: "#6b6b6b",
  },
  blockInfo: {
    width: "70%",
    flexDirection: "row",
    alignItems: "center",
  },
  number: {
    width: "40%",
    fontSize: 11,
  },
  date: {
    width: "55%",
    fontSize: 10,
  },
  bgBar: {
    backgroundColor: "#dedede",
    width: "88%",
    height: 5,
    borderRadius: 20,
    overflow: "hidden",
  },
  bar: {
    // backgroundColor: "red",
    // width: "40%",
    height: 5,
    borderRadius: 20,
  },
});
