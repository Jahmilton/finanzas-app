import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
} from "react-native";
import { capitalize } from "lodash";
import { useNavigation } from "@react-navigation/native";
import getColorByPokemonType from "../utils/getColorByPokemonType";

export default function CuentasCard(props) {
  const { cuenta } = props;
  console.log("Cuenta: ", cuenta);
  const navigation = useNavigation();

  const pokemonColor = getColorByPokemonType(cuenta.tipo);
  const bgStyles = { backgroundColor: pokemonColor, ...styles.bgStyles };

  const goToPokemon = () => {
    navigation.navigate("Pokemon", {
      id: cuenta.id_cuenta,
      nombre: cuenta.nombre,
      descripcion: cuenta.descripcion,
      tipo: cuenta.tipo,
    });
  };

  return (
    <TouchableWithoutFeedback onPress={goToPokemon}>
      <View style={styles.card}>
        <View style={styles.spacing}>
          <View style={bgStyles}>
            <Text style={styles.number}>
              ID: {`${cuenta.id_cuenta}`.padStart(3, 0)}
            </Text>
            <Text style={styles.name}>{capitalize(cuenta.nombre)}</Text>
            <Text style={styles.description}>
              {capitalize(cuenta.descripcion)}
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  card: {
    flex: 1,
    height: 130,
  },
  spacing: {
    flex: 1,
    padding: 5,
  },
  bgStyles: {
    flex: 1,
    borderRadius: 15,
    padding: 10,
  },
  number: {
    position: "absolute",
    right: 10,
    top: 10,
    color: "#fff",
    fontSize: 11,
  },
  name: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 15,
    paddingTop: 10,
  },
  description: {
    color: "#fff",
    fontWeight: "normal",
    fontSize: 12,
    paddingTop: 10,
  },
  image: {
    position: "absolute",
    bottom: 2,
    right: 2,
    width: 90,
    height: 90,
  },
});
