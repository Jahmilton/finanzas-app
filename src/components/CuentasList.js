import React from "react";
import { Text } from "react-native";
import {
  StyleSheet,
  FlatList,
  ActivityIndicator,
  Platform,
} from "react-native";
import CuentasCard from "./CuentasCard";

export default function CuentasList(props) {
  const { pokemons } = props;
  console.log("Lista de cuentas: ", pokemons);

  return (
    <FlatList
      data={pokemons}
      numColumns={2}
      showsVerticalScrollIndicator={false}
      keyExtractor={(pokemon) => String(pokemon.id_cuenta)}
      renderItem={({ item }) => <CuentasCard cuenta={item} />}
      contentContainerStyle={styles.flatListContentContainer}
      onEndReachedThreshold={0.1}
    />
  );
}

const styles = StyleSheet.create({
  flatListContentContainer: {
    paddingHorizontal: 5,
    marginTop: Platform.OS === "android" ? 30 : 0,
  },
  spinner: {
    marginTop: 20,
    marginBottom: Platform.OS === "android" ? 90 : 60,
  },
});
