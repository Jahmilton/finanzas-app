import React from "react";
import { Image } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Icon from "react-native-vector-icons/FontAwesome5";
import FavoriteNavigation from "./FavoriteNavigation";
import CuentaNavigation from "./CuentaNavigation";
import AccountNavigation from "./AccountNavigation";
import IngresosNavigation from "./IngresosNavigation";
import GastosNavigation from "./GastosNavigation";

const Tab = createBottomTabNavigator();

export default function Navigation() {
  return (
    <Tab.Navigator initialRouteName="Account">
      <Tab.Screen
        name="Cuentas"
        component={CuentaNavigation}
        options={{
          tabBarLabel: "Cuentas",
          tabBarIcon: ({ color, size }) => (
            <Icon name="money-check-alt" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Ingresos"
        component={IngresosNavigation}
        options={{
          tabBarLabel: "Ingresos",
          tabBarIcon: ({ color, size }) => (
            <Icon name="plus" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Gastos"
        component={GastosNavigation}
        options={{
          tabBarLabel: "Gastos",
          tabBarIcon: ({ color, size }) => (
            <Icon name="minus" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Favorite"
        component={FavoriteNavigation}
        options={{
          tabBarLabel: "Favoritos",
          tabBarIcon: ({ color, size }) => (
            <Icon name="heart" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Account"
        component={AccountNavigation}
        options={{
          tabBarLabel: "Mi cuenta",
          tabBarIcon: ({ color, size }) => (
            <Icon name="user" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
