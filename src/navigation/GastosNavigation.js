import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import GastosScreen from "../screens/Gastos";

const Stack = createStackNavigator();

export default function IngresosNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Gastos"
        component={GastosScreen}
        options={{ title: "Gastos" }}
      />
    </Stack.Navigator>
  );
}
