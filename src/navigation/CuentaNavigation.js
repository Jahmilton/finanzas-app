import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import CuentasScreen from "../screens/Cuentas";
import PokemonScreen from "../screens/Pokemon";

const Stack = createStackNavigator();

export default function CuentaNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Favorite"
        component={CuentasScreen}
        options={{ title: "Cuentas" }}
      />
      <Stack.Screen
        name="Pokemon"
        component={PokemonScreen}
        options={{
          title: "",
          headerTransparent: true,
        }}
      />
    </Stack.Navigator>
  );
}
