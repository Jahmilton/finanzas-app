import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import IngresosScreen from "../screens/Ingresos";

const Stack = createStackNavigator();

export default function IngresosNavigation() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Ingresos"
        component={IngresosScreen}
        options={{ title: "Ingresos" }}
      />
    </Stack.Navigator>
  );
}
